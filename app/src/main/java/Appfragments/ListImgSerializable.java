package Appfragments;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Geferson on 23/07/2015.
 */
public class ListImgSerializable implements Serializable {
    public static final String LIST_IMG = "ListImgSerializable";
    public  List<Bitmap> imgs;

    public ListImgSerializable(List<Bitmap>imgs){
        this.imgs = imgs;
    }


}
