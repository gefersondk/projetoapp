package Appfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



import java.util.ArrayList;
import java.util.List;

import Appfragments.fragmentsHomeSwitch.FragmentContact;
import Appfragments.fragmentsHomeSwitch.FragmentILiked;
import Appfragments.fragmentsHomeSwitch.FragmentMerchans;
import Appfragments.fragmentsHomeSwitch.FragmentStoryMerchans;
import extras.SlidingTabLayout;
import pop.grd.br.pop.R;

/**
 * Created by Geferson on 21/07/2015.
 */
public class FragmentHomeSwitch extends Fragment {
    static final int NUM_ITEMS = 3;

    private MyAdapter mAdapter;

    private ViewPager mPager;

    private List<Fragment> fragments;



    private SlidingTabLayout tabs;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        fragments = getFragments();
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_pager_home, container, false);
        //ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_pager_home,null);
        mAdapter = new MyAdapter(getActivity().getSupportFragmentManager(), fragments, getActivity());



        mPager = (ViewPager)view.findViewById(R.id.pagerHome);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(2);//quantidade de paginas mantidas para ambos os lados

        tabs = (SlidingTabLayout) view.findViewById(R.id.slidingTabLayoutTabs);
        tabs.setCustomTabView(R.layout.tv_tabs_view,R.id.tv_tab_view);
        tabs.setDistributeEvenly(true);//ocupar o mesmo tamanho em 100 da tela usar antes de setar o pager
        tabs.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        tabs.setSelectedIndicatorColors(getResources().getColor(android.R.color.white));
        tabs.setViewPager(mPager);



        //setRetainInstance(true);
        return view;
    }


    public List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        //fList.add(new FragmentStoryMerchans());
        fList.add(new FragmentMerchans());
        fList.add(new FragmentContact());
        fList.add(new FragmentILiked());

        return fList;
    }


    /*public static class MyAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;
        public MyAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }



    }*/

    public static class MyAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragments;
        private Context context;
        //private String titleTabs [] = {"MERCHANS","CONTACTS","CHAT"};
        private String titleTabs [] = {"MERCHANS","CONTACTS","LIKED"};
        public MyAdapter(FragmentManager fm, List<Fragment> fragments, Context c) {
            super(fm);
            this.fragments = fragments;
            this.context = c;
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            //LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            //fragments.get(position).getView().setLayoutParams(params);
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.titleTabs[position];
        }
    }
}
