package Appfragments;

import android.support.v4.app.Fragment;

import java.io.Serializable;

/**
 * Created by Geferson on 23/07/2015.
 */
public class FragmentHomeSwitchSerializable implements Serializable {

    public static final String FHSS = "FragmentHomeSwitch";

    //private FragmentHomeSwitch fragmentHomeSwitch;
    private Fragment fragmentHomeSwitch;
    public FragmentHomeSwitchSerializable(Fragment f){
        this.fragmentHomeSwitch = f;
    }

    public Fragment getFragmentHomeSwitch(){
        return this.fragmentHomeSwitch;
    }
}
