package Appfragments.fragmentsHomeSwitch;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.nispok.snackbar.listeners.ActionClickListener;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterCardViewMerchan;
import butterknife.Bind;
import butterknife.ButterKnife;
import model.UserMerchan;
import pop.grd.br.pop.R;
import utilities.NetWorkUtil;

/**
 * Created by Geferson on 21/07/2015.
 */
public class FragmentILiked extends Fragment {
    private List<UserMerchan> lista = new ArrayList<>();
    @Bind(R.id.swipeRefreshLayoutILike)
    protected SwipeRefreshLayout swipeRefreshLayoutILike;
    protected View viewMerchan;
    private AdapterCardViewMerchan adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //View viewOffline = inflater.inflate(R.layout.fragment_chat, container, false);
        //viewOffline.setBackgroundColor(Color.CYAN);
        //return viewOffline;

        viewMerchan = inflater.inflate(R.layout.layout_card_merchan, container, false);
        ButterKnife.bind(this, viewMerchan);
        RecyclerView recList = (RecyclerView) viewMerchan.findViewById(R.id.card_list);
        recList.setHasFixedSize(true);
        //LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        //llm.setOrientation(LinearLayoutManager.VERTICAL);
        LinearLayoutManager ll2 = new GridLayoutManager(getActivity(),2);
        //recList.setLayoutManager(llm);
        recList.setLayoutManager(ll2);
        for(int i = 0; i < 10; i++){
            lista.add(new UserMerchan("pedro","iaidsj"));
        }
        //AdapterCardViewMerchan adapter = new AdapterCardViewMerchan(getActivity().getApplicationContext(),lista, getActivity());
        adapter = new AdapterCardViewMerchan(getActivity().getApplicationContext(),lista, getActivity());
        recList.setAdapter(adapter);

        swipeRefreshLayoutILike.setColorSchemeColors(getResources().getColor(R.color.accent));


        return viewMerchan;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        //swipeRefreshLayoutILike.setColorSchemeColors(R.color.pink01);
        swipeRefreshLayoutILike.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //TODO verificar se tem internet
                if(NetWorkUtil.isConnected(getActivity())){
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            SystemClock.sleep(3000);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    getILiked();
                                    adapter.notifyDataSetChanged();
                                    swipeRefreshLayoutILike.setRefreshing(false);
                                }
                            });
                        }
                    }).start();
                }else{
                    swipeRefreshLayoutILike.setRefreshing(false);
                    SnackbarManager.show(
                            Snackbar.with(getActivity().getApplicationContext()) // context
                                    .text(R.string.no_network) // text to display
                                    .actionLabel(R.string.connect) // action button label
                                    .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                                    .type(SnackbarType.MULTI_LINE)
                                    .actionColor(getResources().getColor(android.R.color.white))
                                    .actionListener(new ActionClickListener() {
                                        @Override
                                        public void onActionClicked(Snackbar snackbar) {
                                            Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                                            startActivity(intent);
                                        }
                                    })
                            , (ViewGroup)viewMerchan);
                }
            }
        });
    }

    public void getILiked(){
        for(int i = 0; i < 3; i++){
            lista.add(new UserMerchan("pedro","iaidsj"));
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        setRetainInstance(false);
    }
}
