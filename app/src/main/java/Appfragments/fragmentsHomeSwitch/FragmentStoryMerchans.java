package Appfragments.fragmentsHomeSwitch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import pop.grd.br.pop.R;
import utilities.NetWorkUtil;

/**
 * Created by Geferson on 21/07/2015.
 */
public class FragmentStoryMerchans extends Fragment {
    private ProgressBar progressBarMain;
    private Handler handler;
    private ImageView img;
    private Bitmap imgBitmap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("script verifica"," entrou oncreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewOffline = inflater.inflate(R.layout.fragment_story, container, false);
        //View viewOffline = inflater.inflate(R.layout.fragment_story, null);
        //viewOffline.setBackgroundColor(Color.BLUE);
        handler = new Handler();
        progressBarMain = (ProgressBar) viewOffline.findViewById(R.id.progressBarMain);
        img = (ImageView) viewOffline.findViewById(R.id.imgTeste);
        //viewOffline.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        Log.i("script verifica"," entrou oncreateview");

        setRetainInstance(true);
        return viewOffline;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null){
            downladImagem("https://www.airdroid.com/1507081451/img/home/logoVideo.png");
            Log.i("script verifica"," entrou if");
        }else{
            Log.i("script verifica"," entrou else");
            atualizaTela(imgBitmap);
        }
    }

    private void downladImagem(final String url2){
        new Thread(){
            @Override
            public void run() {
                //super.run();
                try{
                    if(NetWorkUtil.hasActiveInternetConnection()){
                        java.net.URL url = new URL(url2);
                        InputStream is = url.openStream();
                        //final Bitmap imagem = BitmapFactory.decodeStream(is);
                        imgBitmap = BitmapFactory.decodeStream(is);
                        is.close();
                        //atualiza a tela
                        //atualizaTela(imagem);
                        atualizaTela(imgBitmap);
                    }else{
                        atualizaTela(BitmapFactory.decodeResource(getResources(),R.drawable.off));
                    }

                }
                catch (MalformedURLException e){
                    return;
                }
                catch (IOException e){//Uma aplicação real deveria tratar esses erro
                    //Log.e("Erro ao fazer o download.",e.getMessage(),e);
                   // ImageView i = new ImageView(ProgressDialodBaixandoImagem.this);
                    //Toast t = new Toast(ProgressDialodBaixandoImagem.this);
                    //t.setView(i);
                   // t.show();
                    //i.setLayoutParams(new ViewGroup.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT));
                }
            }
        }.start();
    }


    //utiliza um hnudler pra atualizar a tela pois criamos uma thread
    private void atualizaTela(final Bitmap imagemb){
        handler.post(new Runnable() {
            @Override
            public void run() {
                //fecha a janela de progresso
                //dialog.dismiss();
                progressBarMain.setVisibility(View.GONE);
                img.setImageBitmap(imagemb);
                //setRetainInstance(true);
                //ImageView img = (ImageView) findViewById(R.id.imgDownload);

            }
        });
    }
}
