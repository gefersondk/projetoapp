package Appfragments.fragmentsHomeSwitch;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import adapter.interfaceClick.AdapterCardViewMerchans;
import butterknife.Bind;
import butterknife.ButterKnife;
import model.UserMerchan;
import pop.grd.br.pop.R;

/**
 * Created by geferson on 07/09/2015.
 */
public class FragmentMerchans extends Fragment {
    private List<UserMerchan> lista = new ArrayList<UserMerchan>();

    AdapterCardViewMerchans adapter;

    @Bind(R.id.swipeRefreshLayoutILike)
    protected SwipeRefreshLayout swipeRefreshLayoutMerchanHome;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //TRANSITIONS
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Log.i("versao verificar", "lollipop");
            Explode explode = new Explode();
            explode.setDuration(3000);
            Fade fade = new Fade();
            fade.setDuration(3000);

            getActivity().getWindow().setEnterTransition(explode);
            getActivity().getWindow().setReturnTransition(fade);

        }*/

        View viewMerchan = inflater.inflate(R.layout.layout_card_merchan, container, false);
        ButterKnife.bind(this,viewMerchan);

        RecyclerView recList = (RecyclerView) viewMerchan.findViewById(R.id.card_list);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        for(int i = 0; i < 10; i++){
            lista.add(new UserMerchan("pedro","iaidsj"));
        }
        adapter = new AdapterCardViewMerchans(getContext(), lista, getActivity());
        recList.setAdapter(adapter);

        swipeRefreshLayoutMerchanHome.setColorSchemeColors(getResources().getColor(R.color.accent));


        return viewMerchan;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        swipeRefreshLayoutMerchanHome.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try{

                        }catch (Exception e){

                        }
                        swipeRefreshLayoutMerchanHome.setRefreshing(false);
                    }
                },3000);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onPause() {
        super.onPause();
        setRetainInstance(false);
    }
}
