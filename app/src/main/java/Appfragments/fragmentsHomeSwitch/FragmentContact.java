package Appfragments.fragmentsHomeSwitch;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import adapter.AdapterRecyclerviewContacts;
import adapter.interfaceClick.RecyclerViewOnClickListenerContact;
import butterknife.ButterKnife;
import model.UserMerchan;
import pop.grd.br.pop.R;

/**
 * Created by Geferson on 21/07/2015.
 */
public class FragmentContact extends Fragment implements RecyclerViewOnClickListenerContact {
    private List<UserMerchan> userMerchanList;
    //@InjectView(R.id.recycler_view_frame_id)
    protected RecyclerView myRecyclerView;

    private LinearLayoutManager mLayoutManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userMerchanList = getSetUsers(9);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View viewContacts = inflater.inflate(R.layout.fragment_contact, container, false);
        myRecyclerView = (RecyclerView) viewContacts.findViewById(R.id.recycler_view_frame_id);
        //viewOffline.setBackgroundColor(Color.GREEN);
        myRecyclerView.setHasFixedSize(true);//sempre vai ter o mesmo tamanho
        myRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager llm = (LinearLayoutManager) myRecyclerView.getLayoutManager();
                AdapterRecyclerviewContacts adapter = (AdapterRecyclerviewContacts) myRecyclerView.getAdapter();

                if(userMerchanList.size() < 17){
                    if(userMerchanList.size() == llm.findLastCompletelyVisibleItemPosition()+1){//verificar se a ultima posição é do tamanho da lista, implica em ser o ultimo item que esta sendoe exibido
                        List<UserMerchan> listAux = getSetUsers(5);

                        for(int i=0; i < listAux.size(); i++){
                            adapter.addListItem(listAux.get(i), userMerchanList.size());//adiciona depois da ultima pois referencia a mesma do adapterRecyclerviewContacts
                        }
                    }
                }
            }
        });

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //mLayoutManager.setReverseLayout(true);TODO CRIA DE BAIXO PARA CIMA
        myRecyclerView.setLayoutManager(mLayoutManager);
        myRecyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getActivity(),myRecyclerView, (RecyclerViewOnClickListenerContact) this));
        AdapterRecyclerviewContacts adapterContacts = new AdapterRecyclerviewContacts(getActivity(), userMerchanList);
        //metodo na activity fica userMerchanList = getActivity da o cast . getSetUsers();
        myRecyclerView.setAdapter(adapterContacts);


        return viewContacts;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
        Log.i("verificar destroy", "FragmentILike onActivityCreated");
    }

    private List<UserMerchan> getSetUsers(int qnt){
        List<UserMerchan> userMerchanListAux = new ArrayList<UserMerchan>();
        for(int i = 0; i< qnt; i++){
            userMerchanListAux.add(new UserMerchan("name: " + i,"idade: " + i));
        }
        return userMerchanListAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        Toast.makeText(getActivity(), "click", Toast.LENGTH_SHORT).show();
        AdapterRecyclerviewContacts adapterRecyclerviewContacts = (AdapterRecyclerviewContacts) myRecyclerView.getAdapter();
        //adapterRecyclerviewContacts.removeListItem(position);
    }

    @Override
    public void onLongPressClickListener(View view, int position) {
        Toast.makeText(getActivity(), "Long click", Toast.LENGTH_SHORT).show();
    }


    //utilizada apenas para o click
    private static class RecyclerViewTouchListener implements RecyclerView.OnItemTouchListener{
        private Context context;
        private GestureDetector gestureDetector;
        RecyclerViewOnClickListenerContact recyclerViewOnClickListenerContact;

        public RecyclerViewTouchListener(Context c, final RecyclerView rv, RecyclerViewOnClickListenerContact rVOnCLContact){
            this.context = c;
            this.recyclerViewOnClickListenerContact = rVOnCLContact;
            this.gestureDetector = new GestureDetector(c, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public void onLongPress(MotionEvent e) {
                    super.onLongPress(e);
                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerContact != null){
                        recyclerViewOnClickListenerContact.onLongPressClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    View v = rv.findChildViewUnder(e.getX(),e.getY());
                    if(v != null && recyclerViewOnClickListenerContact != null){
                        recyclerViewOnClickListenerContact.onClickListener(v, rv.getChildPosition(v));//pegar posição da view
                    }

                    return true;
                }
            });

        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            gestureDetector.onTouchEvent(e);
            //Toast.makeText(context, "onInterceptTouchEvent",Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            //Toast.makeText(context, "onTouchEvent",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            //Toast.makeText(context, "onRequestDisallowInterceptTouchEvent",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("verificar destroy", "FragmentContact DES");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i("verificar destroy", "FragmentContact DV");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("verificar destroy", "FragmentContact OS");
    }

    @Override
    public void onPause() {
        super.onPause();
        setRetainInstance(false);
        Log.i("verificar destroy", "FragmentContact pause");
    }
}
