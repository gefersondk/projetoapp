package Appfragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import pop.grd.br.pop.R;

/**
 * Created by Geferson on 20/07/2015.
 */
public class SettingsFragment extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View viewSettings = inflater.inflate(R.layout.settings_fragment, container, false);
        getActivity().setTitle("Settings");
        return viewSettings;
    }
}
