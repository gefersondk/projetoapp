package Appfragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import pop.grd.br.pop.R;

/**
 * Created by Geferson on 20/07/2015.
 */
public class AboutFragment extends android.support.v4.app.Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View viewAbout = inflater.inflate(R.layout.about_fragment,container,false);
        getActivity().setTitle("About");
        setRetainInstance(true);
        return viewAbout;
    }

    private String getTitle(){
        return "About";
    }
}
