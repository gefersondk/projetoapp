package activitys;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import model.PublicationUser;
import pop.grd.br.pop.MainActivity;
import pop.grd.br.pop.R;

/**
 * Created by geferson on 28/09/2015.
 */
public class Publish_Activity extends AppCompatActivity {
    Uri imageUri;
    ContentValues values;
    public final int PICTURE_RESULT = 0;

    public final int RESULT_LOAD_IMAGE = 7;

    @Bind(R.id.profile_toolbar)
    protected Toolbar toolbar;

    @Bind(R.id.editTitlePublish)
    protected MaterialEditText title;

    @Bind(R.id.editMerchanPublishtext)
    protected MaterialEditText text;

    @Bind(R.id.getImagePublish)
    protected ImageView imagePublish;

    @Bind(R.id.btn_publish)
    protected Button btn_publish;

    protected Bitmap imgC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publis);
        ButterKnife.bind(this);

        if(toolbar != null){
            setSupportActionBar(toolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        imagePublish.setOnClickListener(new CapImage());

        if(savedInstanceState != null){
            try{
                imgC = savedInstanceState.getParcelable("bitmap");
                if(imgC != null)
                    imagePublish.setImageBitmap(imgC);
                Log.i("publish verificar", "oncreate");
            }catch (Exception e){

            }
        }

        btn_publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
        }
        return true;
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        } else {
            return false;
        }
    }

    protected class CapImage implements View.OnClickListener{
        private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
        private Uri fileUri;
        @Override
        public void onClick(View v) {


            final CharSequence[] items = { "Take Photo", "Choose from Library", "Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(Publish_Activity.this);
            builder.setTitle("Add Photo!");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Take Photo")) {

                        if(checkCameraHardware(getApplicationContext())){
                            // create Intent to take a picture and return control to the calling application
                            //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



                            // start the image capture Intent
                            //startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);


                            values = new ContentValues();
                            values.put(MediaStore.Images.Media.TITLE, "New Picture");
                            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
                            imageUri = getContentResolver().insert(
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                            startActivityForResult(intent, PICTURE_RESULT);

                        }

                    } else if (items[item].equals("Choose from Library")) {
                        Intent i = new Intent(
                                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                        startActivityForResult(i, RESULT_LOAD_IMAGE);
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();




        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if(resultCode == RESULT_OK){
            if(data != null){
                Bundle bundle = data.getExtras();
                if(bundle != null){
                    imgC = (Bitmap) bundle.get("data");
                    if(imgC != null)
                        imagePublish.setImageBitmap(imgC);

                }
            }

        }

        else{
            Toast.makeText(getApplicationContext(), "Canceled", Toast.LENGTH_SHORT).show();
        }*/

        switch (requestCode){
            case PICTURE_RESULT:
                if (requestCode == PICTURE_RESULT) {
                    if (resultCode == Activity.RESULT_OK) {
                        try {
                            imgC = MediaStore.Images.Media.getBitmap(
                                    getContentResolver(), imageUri);
                            imagePublish.setImageBitmap(imgC);
                            String imageurl = getRealPathFromURI(imageUri);
                            Log.i("verificar salvando", "salvou");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
                break;


            case RESULT_LOAD_IMAGE:

                if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    // String picturePath contains the path of selected Image


                    try {
                        imgC = MediaStore.Images.Media.getBitmap(
                                getContentResolver(), selectedImage);
                        imagePublish.setImageBitmap(imgC);
                        String imageurl = getRealPathFromURI(selectedImage);
                        Log.i("verificar salvando", "salvou");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }



    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(imgC != null){
            outState.putParcelable("bitmap", imgC);
            Log.i("publish verificar", "onSave");
        }
        super.onSaveInstanceState(outState);
    }


    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }



}
