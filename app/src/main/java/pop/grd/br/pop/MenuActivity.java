package pop.grd.br.pop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;


import com.facebook.drawee.backends.pipeline.Fresco;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.OnCheckedChangeListener;

import java.util.ArrayList;
import java.util.List;

import Appfragments.AboutFragment;
import Appfragments.FragmentHomeSwitchSerializable;
import Appfragments.HelpFragment;
import Appfragments.OfflineFragment;
import Appfragments.SettingsFragment;
import Appfragments.FragmentHomeSwitch;
import activitys.Publish_Activity;
import butterknife.Bind;
import butterknife.ButterKnife;
import extras.SlidingTabLayout;

/**
 * Created by Geferson on 20/06/2015.
 */
public class MenuActivity extends AppCompatActivity{
    public static final int REQUEST_CODE_PUBLISH = 100;//FragmentActivity

    private static String TAG = "MenuActivity";
    private Context context;
    @Bind(R.id.main_toolbar)
    protected Toolbar tbMain;

    private Fragment fragment;

    //private SlidingTabLayout tabs;

    private String subTitle = "Home";// nome do fragmento no toolbar
    private final String [] SUBTITLES = {"Home","Help","About","Settings"};
    //@InjectView(R.id.inc_toolbar_bottom)
    //protected Toolbar tbBottom;

    /*private boolean isConnected;
    private boolean isAtHomeOfflineFragment = false;

    private BroadcastReceiver netWorkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = NetWorkUtil.getConnectivityTypeString(context);
            Log.i("status conexao", status);

            isConnected = NetWorkUtil.isConnected(context);

            if(isConnected){
                if(isAtHomeOfflineFragment){

                    //isAtHomeOfflineFragment=false;
                    changeFragment(true);

                }

            }
            else{
                changeFragment(false);
            }
        }
    };*/

    //private List<Fragment> fragmentList;


    //private android.support.v4.app.Fragment fragment = null;
    private android.support.v4.app.FragmentManager fragmentManager;

    private OnCheckedChangeListener changeNotify = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(IDrawerItem iDrawerItem, CompoundButton compoundButton, boolean b) {
            return;
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(getApplicationContext());
        setContentView(getLayout());
        ButterKnife.bind(this);
        context = getApplicationContext();
        Log.i("2 verificar problema","não era pra entrar aqui");
        fragmentManager = getSupportFragmentManager();

        //registerConnectionListener();//registrar broadcast

        setSupportActionBar(tbMain);
        //this.tbMain.setSubtitle(subTitle);//TODO DESCOMENTAR CASO VOLTE A USAR

        this.tbMain.setTitle(R.string.titleApp);
        //this.tbMain.setSubtitle(subTitle);
        if(savedInstanceState == null){

            //fragment = new HomeFragment();
            fragment = new FragmentHomeSwitch();
            Log.i("verificar problema","não era pra entrar aqui");

            /*fragmentList = new ArrayList<Fragment>();
            fragmentList.add(new FragmentHomeSwitch());
            fragmentList.add(new HelpFragment());
            //fragmentList.add(new OfflineFragment());
            fragmentList.add(new AboutFragment());
            fragmentList.add(new SettingsFragment());*/


            //this.tbMain.setSubtitle(subTitle);//TODO DESCOMENTAR CASO VOLTE A USAR

            fragmentManager.beginTransaction().replace(R.id.container,fragment).commit();
            Log.i("saved 1","");

        }else {
            //this.tbMain.setSubtitle(savedInstanceState.getString("subtitle"));//TODO DESCOMENTAR CASO VOLTE A USAR
            //FragmentHomeSwitchSerializable frag = (FragmentHomeSwitchSerializable) savedInstanceState.getSerializable(FragmentHomeSwitchSerializable.FHSS);
            //fragment = frag.getFragmentHomeSwitch();
            Log.i("saved","2");
        }


        //this.tbMain.setLogo(R.mipmap.ic_user36);


        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.pink01)
                .addProfiles(
                        new ProfileDrawerItem().withName("Geferson Ribeiro").withEmail("geferson.rd@gmail.com").withIcon(getResources().getDrawable(R.drawable.eu))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

//Now create your drawer and pass the AccountHeader.Result
        final Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(tbMain)
                .withActionBarDrawerToggleAnimated(true)
                .withSavedInstance(savedInstanceState)
                //.withDisplayBelowToolbar(true)
                //.withSliderBackgroundColor(R.color.black_list)
                .withAccountHeader(headerResult)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.home).withIcon(R.mipmap.ic_home_grey600_24dp),
                        //new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.help).withIcon(R.mipmap.ic_help_grey600_24dp),
                        //new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.about).withIcon(R.mipmap.ic_library_grey600_24dp),
                        //new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.settings).withIcon(R.mipmap.ic_settings_grey600_24dp),
                        //new DividerDrawerItem(),
                        new SwitchDrawerItem().withName(R.string.notify).withChecked(true).withIcon(R.mipmap.ic_music_note_grey600_24dp).withOnCheckedChangeListener(changeNotify),
                        //new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.exit).withIcon(R.mipmap.ic_logout_grey600_24dp)

                ).build();//apaga .build e descomenta
                /*.withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {

                        //Fragment fragment = null;
                        // do something with the clicked item :D

                        switch (position) {
                            case 0:
                                fragment = new HomeFragment();
                                Log.i("verificar fragment", "home");
                                break;
                            case 2:
                                fragment = new AboutFragment();
                                Log.i("verificar fragment", "about");
                                break;
                            default:
                                Toast.makeText(context, "Ainda não foi implementado", Toast.LENGTH_SHORT).show();
                                break;
                        }

                        if (fragment != null) {
                            //fragmentManager.beginTransaction().replace(R.id.fragment_home, fragment);
                            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment, "frag");
                            fragmentTransaction.addToBackStack("pilha");
                            fragmentTransaction.commit();
                            Log.i("verificar fragment", "fim commit");
                        }

                        return true;
                    }
                })
                .build();*/

                result.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int position, long l, IDrawerItem iDrawerItem) {
                        //fragment = null;
                        //TODO DESCOMENTAR CASO VOLTE A USAR OS SUBTITLE
                        fragmentManager = getSupportFragmentManager();
                        switch (position) {
                            case 0:
                                fragment = new FragmentHomeSwitch();
                                //fragment = fragmentList.get(position);
                                subTitle = "Home";
                                //setTitle("teste");
                                break;

                            case 1:
                                fragment = new HelpFragment();
                                //fragment = fragmentList.get(position);
                                subTitle = "Help";
                                break;

                            case 2:
                                fragment = new AboutFragment();
                                //fragment = fragmentList.get(position);
                                subTitle = "About";
                                break;
                            case 3:
                                fragment = new SettingsFragment();
                                //fragment = fragmentList.get(position);
                                subTitle = "Settings";
                                break;

                            default:
                                Toast.makeText(context, "Ainda não foi implementado", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        result.closeDrawer();

                        if (fragment != null) {
                            //fragmentManager.beginTransaction().replace(R.id.fragment_home, fragment);
                            //tbMain.setSubtitle(subTitle);//TODO DESCOMENTAR CASO VOLTE A USAR

                            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.container, fragment);
                            //fragmentTransaction.addToBackStack("pilha");
                            fragmentTransaction.commit();

                        }

                        return true;
                    }
                });






    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    /*protected void registerConnectionListener() {

        IntentFilter filter = new IntentFilter();

        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        registerReceiver(netWorkReceiver, filter);

    }*/


    /*private void changeFragment(boolean status){
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(status){

            HomeFragment fragHome= new HomeFragment();
            transaction.replace(R.id.container,fragHome,"");//essa string é se usar findBYIdtag
            //transaction.addToBackStack("pilha");

            transaction.commit();

            //progressWheel.stopSpinning();
            isAtHomeOfflineFragment=false;

        }else{

            OfflineFragment fragOff= new OfflineFragment();
            transaction.replace(R.id.container,fragOff,"");//essa string é se usar findBYIdtag
            //transaction.addToBackStack("pilha");
            transaction.commit();
            isAtHomeOfflineFragment = true;

        }
    }*/

    /*@Override
    protected void onRestart() {
        super.onRestart();
        try{
            registerConnectionListener();
        }catch (Exception e){

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            unregisterReceiver(netWorkReceiver);
        }catch (Exception e){

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            unregisterReceiver(netWorkReceiver);
        }catch (Exception e){

        }

    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.merchan_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_menu_merchan:
                startActivityForResult(new Intent(this, Publish_Activity.class), REQUEST_CODE_PUBLISH);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString("subtitle",subTitle);//TODO DESCOMENTAR CASO VOLTE A USAR
        //outState.putSerializable(FragmentHomeSwitchSerializable.FHSS, new FragmentHomeSwitchSerializable(fragment));

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //subTitle = savedInstanceState.getString("subtitle");//TODO DESCOMENTAR CASO VOLTE A USAR
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public int getLayout(){
        return 0;
    }
}
