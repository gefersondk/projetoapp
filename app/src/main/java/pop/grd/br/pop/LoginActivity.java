package pop.grd.br.pop;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Geferson on 19/06/2015.
 */
public class LoginActivity extends Activity{
    private static String TAG = "LoginActivity";
    @Bind(R.id.btn_login)
    protected Button btnLogin;

    @Bind(R.id.editTextEmail)
    protected EditText email;

    @Bind(R.id.editTextPassword)
    protected EditText password;

    @Bind(R.id.container_form_login)
    protected LinearLayout l_form_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_login);
        ButterKnife.bind(this);
        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        email.setTypeface(face);
        password.setTypeface(face);





    }

    @OnClick(R.id.btn_login)
    protected void login(View view){
        int color = Color.TRANSPARENT;
        Drawable background = view.getRootView().getBackground();
        if (background instanceof ColorDrawable)
            color = ((ColorDrawable) background).getColor();
        Log.i("valor da cor", color+"");
        Log.i("valor da cor", background.toString());
        Log.i("valor da cor", background+"");
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
