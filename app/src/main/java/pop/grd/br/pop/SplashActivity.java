package pop.grd.br.pop;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import utilities.Constants;


public class SplashActivity extends Activity implements Runnable{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentation);
        Handler h = new Handler();
        //solicita para o Handler executar o Runnable (this), fechando a Splash Screen
        //depois de alguns segundos
        h.postDelayed(this, Constants.AppConfig.DELAY);
    }

    @Override
    public void run() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }





}
