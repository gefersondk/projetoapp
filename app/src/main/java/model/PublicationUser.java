package model;

import android.graphics.Bitmap;

/**
 * Created by geferson on 29/09/2015.
 */
public class PublicationUser {

    private int idUser;

    private String title;
    private String text_publication;
    private Bitmap img_publication;
    private boolean isPublic;

    public PublicationUser(String title, String text_publication, Bitmap img_publication, boolean isPublic) {
        this.title = title;
        this.text_publication = text_publication;
        this.img_publication = img_publication;
        this.isPublic = isPublic;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText_publication() {
        return text_publication;
    }

    public void setText_publication(String text_publication) {
        this.text_publication = text_publication;
    }

    public Bitmap getImg_publication() {
        return img_publication;
    }

    public void setImg_publication(Bitmap img_publication) {
        this.img_publication = img_publication;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }
}
