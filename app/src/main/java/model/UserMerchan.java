package model;

import java.util.List;

/**
 * Created by Geferson on 25/07/2015.
 */
public class UserMerchan {

    private String name;
    private String age;
    private String gender;
    private String email;
    private String password;
    private String photo;
    private boolean status;

    private List<PublicationUser> publicationUserList;

    public UserMerchan(){}

    public UserMerchan(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<PublicationUser> getPublicationUserList() {
        return publicationUserList;
    }

    public void setPublicationUserList(List<PublicationUser> publicationUserList) {
        this.publicationUserList = publicationUserList;
    }
}
