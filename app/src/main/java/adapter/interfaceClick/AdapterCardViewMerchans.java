package adapter.interfaceClick;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import activitys.CommentActivity;
import model.UserMerchan;
import pop.grd.br.pop.R;

/**
 * Created by geferson on 07/09/2015. lista primeira tab merchans
 */
public class AdapterCardViewMerchans extends RecyclerView.Adapter<AdapterCardViewMerchans.ViewHolderMerchans> {

    private List<UserMerchan> userMerchansList;
    private Context context;
    private Activity activity;
    private LayoutInflater inflater;
    public AdapterCardViewMerchans(Context context, List<UserMerchan> userMerchanList, Activity activity) {
        this.userMerchansList = userMerchanList;
        this.context = context;
        this.activity = activity;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolderMerchans onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.cardview_merchan_simple_list,parent, false);
        ViewHolderMerchans viewHolderMerchans = new ViewHolderMerchans(view);
        return viewHolderMerchans;
    }

    @Override
    public void onBindViewHolder(ViewHolderMerchans holder, int position) {
        Log.i("crateRecycler", "onBindViewHolder");

        //holder.nickUser.setText(userMerchansList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return this.userMerchansList.size();
    }

    public void addListItem(UserMerchan user, int position){
        userMerchansList.add(user);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        userMerchansList.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolderMerchans extends RecyclerView.ViewHolder{
        public TextView nickUser;
        public ViewHolderMerchans(final View itemView) {
            super(itemView);

            ImageView userName = (ImageView) itemView.findViewById(R.id.id_comment);
            userName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemView.getContext().startActivity(new Intent(itemView.getContext(), CommentActivity.class));
                }
            });

            nickUser = (TextView) itemView.findViewById(R.id.nick_user);

        }


    }
}
