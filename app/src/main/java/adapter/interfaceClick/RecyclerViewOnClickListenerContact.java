package adapter.interfaceClick;

import android.view.View;

/**
 * Created by Geferson on 25/07/2015.
 */
public interface RecyclerViewOnClickListenerContact {

    public void onClickListener(View view, int position);
    public void onLongPressClickListener(View view, int position);
}
