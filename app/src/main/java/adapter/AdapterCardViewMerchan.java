package adapter;

/**
 * Created by Geferson on 30/07/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;
import java.util.Random;

import model.UserMerchan;
import pop.grd.br.pop.MainActivity;
import pop.grd.br.pop.R;

/**
 * Created by Geferson on 25/07/2015. merchans curtidos
 */
public class AdapterCardViewMerchan extends RecyclerView.Adapter<AdapterCardViewMerchan.ViewHolderContact> {

    private List<UserMerchan> userMerchanLis;
    private LayoutInflater inflater;
    public static Context c;
    private static Activity activity;// referencia usada para criar o dialog no click

    public AdapterCardViewMerchan(Context context, List<UserMerchan> userMerchanList, Activity activity){
        this.c = context;
        this.userMerchanLis = userMerchanList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
    }


    @Override
    public ViewHolderContact onCreateViewHolder(ViewGroup parent, int viewType) {//chamada pesada precisa do bind para não precisar criar mais views
        Log.i("crateRecycler", "onCreateViewHolder");//cardview_merchan
        View view = inflater.inflate(R.layout.cardview_photos3, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderContact myViewHolderContact =  new ViewHolderContact(view);
        return myViewHolderContact;
    }

    @Override
    public void onBindViewHolder(ViewHolderContact holder, int position) {
        Log.i("crateRecycler","onBindViewHolder");
//        holder.name.setText(userMerchanLis.get(position).getName());
  //      holder.status.setText(userMerchanLis.get(position).getAge());
    }

    @Override
    public int getItemCount() {
        return userMerchanLis.size();
    }

    public void addListItem(UserMerchan user, int position){
        userMerchanLis.add(user);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        userMerchanLis.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolderContact extends RecyclerView.ViewHolder{

        //@InjectView(R.id.iv_contact)
        public SimpleDraweeView photo;
        //@InjectView(R.id.img_photo_perfil)
        public TextView name;
        public SimpleDraweeView img_pofile;
        //@InjectView(R.id.tv_status_contact)
        public TextView status;//TODO MUDAR PARA STATUS NO SET DEPOIS

        public ViewHolderContact(View itemView) {
            super(itemView);
            img_pofile = (SimpleDraweeView) itemView.findViewById(R.id.img_photo_perfil);
            String url1 = "http://cdn.revistadonna.clicrbs.com.br/wp-content/uploads/2014/04/gabriella-lenzi-e-modelo-e-estudante-de-moda.jpg";
            String url3 = "https://ibopetvaudiencia.files.wordpress.com/2011/11/talula-modelo_f_002.jpg";
            String url5 = "http://s2.glbimg.com/-3gqJnOROqQrVpWkKu_T3pOFVw0=/s.glbimg.com/jo/g1/f/original/2014/09/17/loranne.jpg";
            Random r = new Random();
            String urlF;
            int i = r.nextInt(3);
            switch (i){
                case 0:
                    urlF = url1;
                    break;
                case 1:
                    urlF = url3;
                    break;
                case 2:
                    urlF = url5;
                    break;
                case 3:
                    urlF = url5;
                    break;
                default:
                    urlF = url1;
            }
            Uri uri = Uri.parse(urlF);

            img_pofile.setImageURI(uri);

            //Captura o container com nome e merchan para o click
            RelativeLayout container_details_iliked = (RelativeLayout) itemView.findViewById(R.id.container_details_iliked);
            container_details_iliked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            new MaterialDialog.Builder(activity)
                                    .title("Paula")
                                    .content("Vender arte das coisa que a natureza da todos os dias. Curte ai que adiciono, garanto altas viagens.")
                                    .positiveText("Ok")
                                    //.iconRes(R.drawable.profile2)
                                    .icon(img_pofile.getDrawable().getCurrent())
                                            .maxIconSize(100)
                                    //.negativeText("vermelho")
                                    .show();
                        }
                    });
                }
            });
            //name = (TextView) itemView.findViewById(R.id.tv_name_contact);
            //status = (TextView) itemView.findViewById(R.id.tv_status_contact);

            //Typeface face= Typeface.createFromAsset(c.getAssets(), "fonts/Roboto-Regular.ttf");
            //name.setTypeface(face);

            //photo = (SimpleDraweeView) itemView.findViewById(R.id.iv_contact);
            //Uri uri = Uri.parse("https://raw.githubusercontent.com/facebook/fresco/gh-pages/static/fresco-logo2.png");
            //photo.setImageURI(uri);

            //ButterKnife.inject(itemView);
        }


    }



}
