package adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import adapter.interfaceClick.RecyclerViewOnClickListenerContact;
import butterknife.ButterKnife;
import model.UserMerchan;
import pop.grd.br.pop.R;

/**
 * Created by Geferson on 25/07/2015.
 */
public class AdapterRecyclerviewContacts extends RecyclerView.Adapter<AdapterRecyclerviewContacts.ViewHolderContact> {

    private List<UserMerchan> userMerchanLis;
    private LayoutInflater inflater;
    public static Context c;

    public AdapterRecyclerviewContacts(Context context, List<UserMerchan> userMerchanList){
        this.c = context;
        this.userMerchanLis = userMerchanList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ViewHolderContact onCreateViewHolder(ViewGroup parent, int viewType) {//chamada pesada precisa do bind para não precisar criar mais views
        Log.i("crateRecycler","onCreateViewHolder");
        View view = inflater.inflate(R.layout.item_contact, parent, false);//parent é o recyclerview. false pra usar o layout params do recyclerview
        ViewHolderContact myViewHolderContact =  new ViewHolderContact(view);
        return myViewHolderContact;
    }

    @Override
    public void onBindViewHolder(ViewHolderContact holder, int position) {
        Log.i("crateRecycler","onBindViewHolder");
        holder.name.setText(userMerchanLis.get(position).getName());
        holder.status.setText(userMerchanLis.get(position).getAge());
    }

    @Override
    public int getItemCount() {
        return userMerchanLis.size();
    }

    public void addListItem(UserMerchan user, int position){
        userMerchanLis.add(user);
        notifyItemInserted(position);
    }

    public void removeListItem(int position){
        userMerchanLis.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolderContact extends RecyclerView.ViewHolder{

        //@InjectView(R.id.iv_contact)
        public SimpleDraweeView photo;
        //@InjectView(R.id.tv_name_contact)
        public TextView name;
        //@InjectView(R.id.tv_status_contact)
        public TextView status;//TODO MUDAR PARA STATUS NO SET DEPOIS

        public ViewHolderContact(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.tv_name_contact);
            status = (TextView) itemView.findViewById(R.id.tv_status_contact);

            //Typeface face= Typeface.createFromAsset(c.getAssets(), "fonts/Roboto-Regular.ttf");
            //name.setTypeface(face);

            photo = (SimpleDraweeView) itemView.findViewById(R.id.iv_contact);
            //Uri uri = Uri.parse("https://raw.githubusercontent.com/facebook/fresco/gh-pages/static/fresco-logo2.png");
            //photo.setImageURI(uri);

            //ButterKnife.inject(itemView);
        }


    }



}
